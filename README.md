Murano CKAN
===========

This is the source for the CKAN Murano package running on the NeCTAR cloud.

A Makefile is included to help the build process.

You will need your NeCTAR cloud credentials loaded and the Murano CLI tools
available in your path.

You will also need the following required packages:

* [DuplyBackups](https://bitbucket.org/qcif-murano/murano-duply-backups)
* [QRIScloudLib](https://bitbucket.org/qcif-murano/murano-qriscloud-lib)
